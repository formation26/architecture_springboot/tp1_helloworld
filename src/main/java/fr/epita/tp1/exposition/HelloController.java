package fr.epita.tp1.exposition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tp1.application.HelloService;

@RestController
@RequestMapping("/hello")
public class HelloController {
	
	@Autowired
	HelloService service;
	
	@GetMapping
	public String getMessage() {
		return service.getMessage();
	}

}
